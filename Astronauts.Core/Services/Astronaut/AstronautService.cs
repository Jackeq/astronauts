﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Astronauts.Core.Exceptions;
using Astronauts.Dal;
using Microsoft.EntityFrameworkCore;

namespace Astronauts.Core.Services.Astronaut
{
    public class AstronautService : IAstronautService
    {
        private AstronautsContext AstronautsContext { get; set; }
        
        public AstronautService(AstronautsContext astronautsContext)
        {
            AstronautsContext = astronautsContext;
        }

        public async Task<Dal.Entities.Astronaut> GetAstronautAsync(int id)
        {
            var astronaut = await AstronautsContext.Astronauts.FindAsync(id);
            if (astronaut is null)
            {
                throw new EntityNotFoundException("Astronaut not found.");
            }

            return astronaut;
        }
        
        public async Task<List<Dal.Entities.Astronaut>> GetAllAstronautsAsync(int offset = 0, int limit = 20)
        {
            return await AstronautsContext.Astronauts.AsQueryable().Skip(offset).Take(limit).ToListAsync();
        }
        
        public async Task<Dal.Entities.Astronaut> CreateAstronautAsync(Dal.Entities.Astronaut astronaut)
        {
            await AstronautsContext.Astronauts.AddAsync(astronaut);
            await AstronautsContext.SaveChangesAsync();

            return astronaut;
        }
        
        public async Task DeleteAstronautAsync(int id)
        {
            var astronaut = await AstronautsContext.Astronauts.FindAsync(id);
            if (astronaut is null)
            {
                throw new EntityNotFoundException("Astronaut not found");
            }
            AstronautsContext.Astronauts.Remove(astronaut);
            await AstronautsContext.SaveChangesAsync();
        }
        
        public async Task<Dal.Entities.Astronaut> EditAstronautAsync(int id, Dal.Entities.Astronaut astronautNew)
        {
            var astronautOld = await AstronautsContext.Astronauts.FindAsync(id);
            if (astronautOld is null)
            {
                throw new EntityNotFoundException("Astronaut not found");
            }

            astronautNew.Id = astronautOld.Id;
            AstronautsContext.Entry(astronautOld).CurrentValues.SetValues(astronautNew);
            await AstronautsContext.SaveChangesAsync();

            return astronautNew;
        }
    }
}