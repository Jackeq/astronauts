﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astronauts.Core.Services.Astronaut
{
    public interface IAstronautService
    {
        Task<Dal.Entities.Astronaut> GetAstronautAsync(int id);
        
        Task<Dal.Entities.Astronaut> CreateAstronautAsync(Dal.Entities.Astronaut astronaut);
        
        Task DeleteAstronautAsync(int id);

        Task<Dal.Entities.Astronaut> EditAstronautAsync(int id, Dal.Entities.Astronaut astronautNew);

        Task<List<Dal.Entities.Astronaut>> GetAllAstronautsAsync(int offset = 0, int limit = 20);
    }
}