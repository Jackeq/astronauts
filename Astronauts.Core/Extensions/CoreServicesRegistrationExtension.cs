using Astronauts.Core.Services.Astronaut;
using Microsoft.Extensions.DependencyInjection;

namespace Astronauts.Core.Extensions
{
    public static class CoreServicesRegistrationExtension
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services)
        {
            services.AddTransient<IAstronautService, AstronautService>();
            return services;
        }
    }
}