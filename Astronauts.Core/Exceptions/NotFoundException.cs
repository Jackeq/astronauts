﻿using System;

namespace Astronauts.Core.Exceptions
{
    public class AstronautsExceptions : Exception
    {
        public AstronautsExceptions()
        {
        }
			
        public AstronautsExceptions(string message) : base(message)
        {
        }
    }
    
    public class EntityNotFoundException : AstronautsExceptions
    {
        public EntityNotFoundException()
        {
        }
			
        public EntityNotFoundException(string message) : base(message)
        {
        }
    }
}