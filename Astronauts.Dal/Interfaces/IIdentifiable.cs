﻿namespace Astronauts.Dal.Interfaces
{
    public interface IIdentifiable
    {
        public int Id { get; set; }
    }
}