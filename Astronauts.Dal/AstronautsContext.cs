using Astronauts.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Astronauts.Dal
{
    public class AstronautsContext : DbContext
    {
        public DbSet<Astronaut> Astronauts { get; set; } = null!;

        public AstronautsContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Astronaut>(b =>
            {
                b.Property(s => s.Name).IsRequired();
                b.Property(s => s.Surname).IsRequired();
                b.Property(s => s.BirthDate).IsRequired();
                b.Property(s => s.Superpower).IsRequired();
            });
        }
    }
}