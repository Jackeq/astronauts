using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Astronauts.Dal.Extensions
{
    public static class DatabaseServiceConfigurationExtension
    {
        private static DbContextOptionsBuilder BuildOptions(string connectionString,
            DbContextOptionsBuilder? builder = null)
        {
            builder ??= new DbContextOptionsBuilder();

            builder
                .UseLazyLoadingProxies()
                .UseNpgsql(connectionString);

            return builder;
        }

        public static IServiceCollection AddDatabase(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<AstronautsContext>(builder => BuildOptions(connectionString, builder));
            services.AddScoped<DbContext, AstronautsContext>(provider => provider.GetService<AstronautsContext>()!);
            return services;
        }
    }
}