﻿using System;
using Astronauts.Dal.Interfaces;

namespace Astronauts.Dal.Entities
{
    public class Astronaut : IIdentifiable
    {
        public int Id { get; set; }
        
        public string Name { get; set; } = null!;

        public string Surname { get; set; } = null!;

        public DateTime BirthDate { get; set; }

        public string Superpower { get; set; } = null!;
    }
}