using System;
using System.Threading;
using System.Threading.Tasks;
using Astronauts.Dal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Astronauts.Migrations
{
    public class MigrationWorker : BackgroundService
    {
        private readonly IServiceProvider serviceProvider;

        public MigrationWorker(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await using var context = serviceProvider.CreateScope().ServiceProvider.GetService<AstronautsContext>()!;
            await context.Database.MigrateAsync(cancellationToken: stoppingToken);
            await context.Database.EnsureCreatedAsync(stoppingToken);
        }
    }
}