using System;
using System.Threading.Tasks;
using Astronauts.Dal.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Astronauts.Migrations
{
    public class App : Runnable.App
    {
        public App(string[] args, string? environment) : base(args, environment)
        {
        }

        public override async Task<int> Run()
        {
            try
            {
                await Host.CreateDefaultBuilder(Args)
                    .ConfigureAppConfiguration(builder => builder.AddConfiguration(Configuration))
                    .UseEnvironment(Environment)
                    .ConfigureServices(services =>
                    {
                        services.AddDatabase(Configuration.GetConnectionString("Pgsql"));
                        services.AddHostedService<MigrationWorker>();
                    })
                    .Build()
                    .RunAsync();
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Host terminated unexpectedly, exception: " + ex.Message);
                return 1;
            }
        }
    }
}