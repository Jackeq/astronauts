﻿using System;
using Astronauts.Migrations;

var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
return await new App(args, environment).Run();