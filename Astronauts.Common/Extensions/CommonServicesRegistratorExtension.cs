using Microsoft.Extensions.DependencyInjection;

namespace Astronauts.Common.Extensions
{
    public static class CommonServicesRegistratorExtension
    {
        public static IServiceCollection AddCommonServices(this IServiceCollection services)
        {
            return services;
        }
    }
}