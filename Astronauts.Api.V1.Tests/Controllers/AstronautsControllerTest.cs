﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Astronauts.Api.V1.Controllers;
using Astronauts.Api.V1.DTO;
using Astronauts.Core.Exceptions;
using Astronauts.Core.Services.Astronaut;
using Astronauts.Dal;
using Astronauts.Dal.Tests.Hotel.Dal.Tests.Api.V1.Helpers;
using FluentValidation.TestHelper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Astronauts.Api.V1.Tests.Controllers
{
    [TestClass]
    public class AstronautsControllerTest : ControllerTest
    {
        private readonly AstronautDto.Validator AstronautValidator = new();
        private AstronautsController Controller { get; set; }
        private AstronautsContext AstronautsContext { get; set; }
        private AstronautService AstronautService { get; set; }

        [TestInitialize]
        public void Init()
        {
            AstronautsContextTesting.RecreateDatabase();
            AstronautsContext = AstronautsContextTesting.CreateContext();
            AstronautService = new AstronautService(AstronautsContext);
            Controller = new AstronautsController(Mapper, AstronautService);
        }

        [TestMethod]
        public void Validation_EmptyValues_ShouldFail()
        {
            var dto = new AstronautDto.Create();
            var result = AstronautValidator.TestValidate(dto);
            result.ShouldHaveValidationErrorFor(create => create.Name);
            result.ShouldHaveValidationErrorFor(create => create.Birthdate);
            result.ShouldHaveValidationErrorFor(create => create.Surname);
            result.ShouldHaveValidationErrorFor(create => create.Superpower);
        }

        [TestMethod]
        public void Validation_InvalidValues_ShouldFail()
        {
            var dto = new AstronautDto.Create
            {
                Birthdate = new DateTime(), Name = "", Surname = "", Superpower = ""
            };
            var result = AstronautValidator.TestValidate(dto);
            result.ShouldHaveValidationErrorFor(create => create.Name);
            result.ShouldHaveValidationErrorFor(create => create.Surname);
            result.ShouldHaveValidationErrorFor(create => create.Birthdate);
            result.ShouldHaveValidationErrorFor(create => create.Superpower);
        }


        [TestMethod]
        public async Task GetOne_ByCorrectId_ShouldSucceed()
        {
            var dto = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };

            await Controller.PostAsync(dto);
            var get = await Controller.GetAsync(1);
            Assert.IsInstanceOfType(get.Result, typeof(OkObjectResult));
            var result = get.Result as OkObjectResult;
            Assert.IsInstanceOfType(result!.Value, typeof(AstronautDto.Read));
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException), "Astronaut not found.")]
        public async Task GetOne_ByIncorrectId_ShouldFail()
        {
            var result = await Controller.GetAsync(1);
        }

        [TestMethod]
        public async Task GetMany_ShouldSucceed()
        {
            var dto = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };
            var dto2 = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };
            var dto3 = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };
            Assert.IsInstanceOfType((await Controller.PostAsync(dto)).Result, typeof(OkObjectResult));
            Assert.IsInstanceOfType((await Controller.PostAsync(dto2)).Result, typeof(OkObjectResult));
            Assert.IsInstanceOfType((await Controller.PostAsync(dto3)).Result, typeof(OkObjectResult));

            var get = await Controller.GetAllAsync(0, 20);
            Assert.IsInstanceOfType(get.Result, typeof(OkObjectResult));
            var result = get.Result as OkObjectResult;
            Assert.IsInstanceOfType(result!.Value, typeof(List<AstronautDto.Read>));
            var collection = result.Value as List<AstronautDto.Read>;
            Assert.AreEqual(3, collection!.Count);
        }

        [TestMethod]
        public async Task DeleteByCorrectId_ShouldSucceed()
        {
            var dto = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };

            var post = await Controller.PostAsync(dto);
            Assert.IsInstanceOfType(post.Result, typeof(OkObjectResult));
            var result = post.Result as OkObjectResult;
            Assert.IsInstanceOfType(result!.Value, typeof(AstronautDto.Read));
            var delete = await Controller.DeleteAsync(1);
            Assert.IsInstanceOfType(delete, typeof(NoContentResult));
        }

        [TestMethod]
        public void Delete_ByIncorrectId_ShouldFail()
        {
            var delete = Controller.DeleteAsync(9999);
            Assert.AreEqual(delete.IsCompleted, true);
        }

        [TestMethod]
        public async Task Create_CorrectOne_ShouldSucceed()
        {
            var dto = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };

            var post = await Controller.PostAsync(dto);
            Assert.IsInstanceOfType(post.Result, typeof(OkObjectResult));
            var result = post.Result as OkObjectResult;
            Assert.IsInstanceOfType(result!.Value, typeof(AstronautDto.Read));
            var valueDto = result.Value as AstronautDto.Read;
            Assert.AreEqual("Ferko", valueDto!.Name);
        }

        [TestMethod]
        public async Task Edit_ByCorrectId_ShouldSucceed()
        {
            var dto = new AstronautDto.Create
            {
                Name = "Ferko",
                Surname = "Robotnik",
                Birthdate = new DateTime(2000, 08, 08),
                Superpower = "metal detector"
            };
            await Controller.PostAsync(dto);

            var dto2 = new AstronautDto.Create
            {
                Name = "Romanko",
                Surname = "Sziga",
                Birthdate = new DateTime(1990, 09, 09),
                Superpower = "metal detector 100 000x"
            };

            var edit = await Controller.PutAsync(1, dto2);
            Assert.IsInstanceOfType(edit, typeof(OkObjectResult));

            var get = await Controller.GetAsync(1);
            Assert.IsInstanceOfType(get.Result, typeof(OkObjectResult));
            var result = get.Result as OkObjectResult;
            Assert.IsInstanceOfType(result!.Value, typeof(AstronautDto.Read));
            var valueDto = result.Value as AstronautDto.Read;

            Assert.AreNotEqual(valueDto!.Name, dto.Name);
            Assert.AreNotEqual(valueDto.Surname, dto.Surname);
            Assert.AreNotEqual(valueDto.Birthdate, dto.Birthdate);
            Assert.AreNotEqual(valueDto.Superpower, dto.Superpower);
        }
    }
}
