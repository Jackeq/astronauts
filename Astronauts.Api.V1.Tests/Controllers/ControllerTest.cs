using System;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Astronauts.Api.V1.Tests.Controllers
{
    [TestClass]
    public class ControllerTest
    {
        protected static IMapper Mapper { get; set; }

        [AssemblyInitialize]
        public static void InitMapper(TestContext testContext)
        {
            var configuration = new MapperConfiguration(expression =>
            {
                expression.AddMaps(typeof(V1));
                //expression.ConstructServicesUsing(Resolve);
            });

            Mapper = new Mapper(configuration);
        }

        /*private static T Resolve<T>()
        {
            return (T) Resolve(typeof(T));
        }

        private static object Resolve(Type type)
        {
            if (type == typeof(PasswordToHashConverter))
                return new PasswordToHashConverter(Resolve<PasswordService>());
            if (type == typeof(PasswordService))
                return new PasswordService();
            Assert.Fail("Can not resolve type " + type.AssemblyQualifiedName);
            return null;
        }*/
    }
}
