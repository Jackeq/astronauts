using Astronauts.Api.V1.Controllers;
using Astronauts.Api.V1.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Astronauts.Api.V1.Tests.Controllers
{
    [TestClass]
    public class AppControllerTest : ControllerTest
    {
        private static AppController Controller;

        [ClassInitialize]
        public static void CreateController(TestContext testContext)
        {
            Controller = new AppController(Mapper);
        }

// 		[TestMethod] todo read from assembly
        public void CheckVersion_NoInput_ShouldReturnZeros()
        {
            var response = Controller.Get();
            var okResult = response.Result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(AppSettingsDto));
            Assert.AreEqual(okResult.StatusCode, StatusCodes.Status200OK);

            var data = okResult.Value as AppSettingsDto;
            Assert.AreEqual(data?.Version, "0.0.0.0");
        }
    }
}
