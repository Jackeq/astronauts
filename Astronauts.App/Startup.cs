using Astronauts.Api.Extensions;
using Astronauts.Api.V1;
using Astronauts.Api.Versioning;
using Astronauts.App.Location;
using Astronauts.Common.Extensions;
using Astronauts.Core.Extensions;
using Astronauts.Dal.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Astronauts.App
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration!;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabase(Configuration.GetConnectionString("Pgsql"));
            services.AddSingleton<IApiVersion, V1>();
            services.AddSingleton<LocationService>();
            services.AddLocalization();
            services.AddCommonServices();
            services.AddCoreServices();
            services.AddApiServices();
        }

        /// <summary>
        /// Application configuration
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="versionDescriptionProvider"></param>
        /// <param name="locationService"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IApiVersionDescriptionProvider versionDescriptionProvider,
            LocationService locationService, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (env.IsDevelopment())
            {
                app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); });
            }

            app.UseRouting();
            app.UseDefaultFiles();
            app.UseMultiVersionedSwagger(versionDescriptionProvider);
            app.UseEndpoints(e => { e.MapControllers(); });

            var frontendLocation = locationService.PrepareLocation("frontend");
            app.UseSpa(builder =>
            {
                builder.Options!.SourcePath = frontendLocation.AbsolutePath;
                builder.Options!.DefaultPage = $"/{frontendLocation.RelativePath}/index.html";
            });
        }
    }
}
