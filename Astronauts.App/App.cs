using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Astronauts.App
{
    public class App : Runnable.App
    {
        public App(string[] args, string? environment) : base(args, environment)
        {
        }

        public override async Task<int> Run()
        {
            try
            {
                var webHost = WebHost.CreateDefaultBuilder(Args)
                    .UseStartup<Startup>()
                    .UseConfiguration(Configuration)
                    .UseWebRoot("public")
                    .Build();
                await webHost.RunAsync();
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Host terminated unexpectedly, exception: " + ex.Message);
                return 1;
            }
        }
    }
}