﻿using System;
using Astronauts.App;

var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
return await new App(args, environment).Run();