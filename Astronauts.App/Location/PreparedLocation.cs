using System.IO;

namespace Astronauts.App.Location
{
    public class PreparedLocation
    {
        public string RelativePath { get; }

        public string AbsolutePath { get; }

        public PreparedLocation(string relativePath, string rootPath)
        {
            RelativePath = relativePath;
            AbsolutePath = Path.Combine(rootPath, relativePath);
        }
    }
}