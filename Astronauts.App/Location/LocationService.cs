using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Astronauts.App.Location
{
    public class LocationService
    {
        private readonly IWebHostEnvironment env;

        public LocationService(IWebHostEnvironment env)
        {
            this.env = env;
        }

        public PreparedLocation PrepareLocation(string relativePath, bool useWebRoot = true)
        {
            var absolutePath = useWebRoot ? env.WebRootPath : env.ContentRootPath;
            Directory.CreateDirectory(absolutePath);
            return new PreparedLocation(relativePath, absolutePath);
        }
    }
}