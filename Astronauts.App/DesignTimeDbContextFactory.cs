using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Astronauts.Dal;

namespace Astronauts.App
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AstronautsContext>
    {
        public AstronautsContext CreateDbContext(string[] args)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var connectionString = Runnable.App.CreateConfiguration(environment!).GetConnectionString("Pgsql");
            var optionsBuilder = new DbContextOptionsBuilder<AstronautsContext>();
            optionsBuilder
                .UseLazyLoadingProxies()
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
                .UseNpgsql(connectionString);
            return new AstronautsContext(optionsBuilder.Options);
        }
    }
}