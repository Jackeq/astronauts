using Microsoft.AspNetCore.Builder;

namespace Astronauts.App.Extensions
{
    public static class ApplicationBuilderRoutingExtension
    {
        public static IApplicationBuilder UseCustomRouting(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder;
        }
    }
}