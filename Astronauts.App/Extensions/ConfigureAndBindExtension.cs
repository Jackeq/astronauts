using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Astronauts.App.Extensions
{
    public static class ConfigureAndBindExtension
    {
        /// <summary>
        /// Creates dynamic configuration and bind it to instance usable for further configuration.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configurationSection"></param>
        /// <param name="returnValue">Need result?</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Configure<T>(this IServiceCollection services, IConfigurationSection configurationSection,
            bool returnValue) where T : class, new()
        {
            var output = new T();
            services.Configure<T>(configurationSection);
            configurationSection.Bind(output);
            return output;
        }
    }
}