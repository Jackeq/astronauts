using Astronauts.Api.Controllers.Abstract;
using Astronauts.Api.V1.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Astronauts.Api.V1.Controllers
{
    /// <summary>
    /// Application information
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AppController : ApiController
    {
        /// <inheritdoc />
        public AppController(IMapper mapper) : base(mapper)
        {
        }

        /// <summary>
        /// Get Api version from assembly
        /// </summary>
        /// <returns>Version string</returns>
        [HttpGet]
        public ActionResult<AppSettingsDto> Get()
        {
            var version = GetType().Assembly.GetName().Version?.ToString();
            return Ok(Mapper.Map<AppSettingsDto>(version));
        }
    }
}