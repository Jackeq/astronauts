﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Astronauts.Api.Controllers.Abstract;
using Astronauts.Api.V1.DTO;
using Astronauts.Core.Exceptions;
using Astronauts.Core.Services.Astronaut;
using Astronauts.Dal.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Astronauts.Api.V1.Controllers
{
    /// <summary>
    /// Astronaut management
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AstronautsController : ApiController
    {
        private readonly IAstronautService AstronautService;
        
        public AstronautsController(IMapper mapper, IAstronautService astronautService) : base(mapper)
        {
            AstronautService = astronautService;
        }
        
        /// <summary>
        /// Get Astronaut by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>		
        [HttpGet("{id:int}")]
        public async Task<ActionResult<AstronautDto.Read>> GetAsync(int id)
        {
            try
            {
                return Ok(Mapper.Map<AstronautDto.Read>(await AstronautService.GetAstronautAsync(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Get All Astronauts
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>		
        [HttpGet]
        public async Task<ActionResult<List<AstronautDto.Read>>> GetAllAsync(int offset = 0, int limit = 20)
        {
            return Ok(Mapper.Map<List<AstronautDto.Read>>(await AstronautService.GetAllAstronautsAsync(offset, limit)));
        }

        /// <summary>
        /// Create Astronaut
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>		
        [HttpPost]
        public async Task<ActionResult<AstronautDto.Read>> PostAsync(AstronautDto.Create dto)
        {
            return Ok(Mapper.Map<AstronautDto.Read>(await AstronautService.CreateAstronautAsync(Mapper.Map<Astronaut>(dto))));
        }
        
        /// <summary>
        /// Delete Astronaut by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>		
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await AstronautService.DeleteAstronautAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Edit Astronaut by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>		
        [HttpPut("{id:int}")]
        public async Task<ActionResult> PutAsync(int id, AstronautDto.Create dto)
        {
            try
            {
                return Ok(Mapper.Map<AstronautDto.Read>(await AstronautService.EditAstronautAsync(id, Mapper.Map<AstronautDto.Create, Astronaut>(dto))));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound();
            }
        }
    }
}
