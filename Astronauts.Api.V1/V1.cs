using Astronauts.Api.Versioning;

namespace Astronauts.Api.V1
{
    /// <summary>
    /// V1 Api version
    /// </summary>
    public sealed class V1 : IApiVersion
    {
    }
}