using System;
using AutoMapper;

namespace Astronauts.Api.V1.DTO
{
    public class AppSettingsDto
    {
        public string? Version { get; set; }

        public class DtoProfile : Profile
        {
            public DtoProfile()
            {
                CreateMap<string, AppSettingsDto>()
                    .ForMember(t => t.Version,
                        expression => expression.MapFrom(t => t));
                CreateMap<Version, AppSettingsDto>()
                    .ForMember(t => t.Version,
                        expression => expression.MapFrom(t => t.ToString()));
            }
        }
    }
}