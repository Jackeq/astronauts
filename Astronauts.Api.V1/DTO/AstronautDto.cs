﻿using System;
using Astronauts.Dal.Entities;
using AutoMapper;
using FluentValidation;

namespace Astronauts.Api.V1.DTO
{
    public class AstronautDto
    {
        public string Name { get; set; } = null!;

        public string Surname { get; set; } = null!;

        public DateTime Birthdate { get; set; }

        public string Superpower { get; set; } = null!;

        public class Read : AstronautDto
        {
            public int Id { get; set; }
        }
        
        public class Create : AstronautDto
        {
            
        }
        
        public class DtoProfile : Profile
        {
            public DtoProfile()
            {
                CreateMap<Astronaut, Read>();
                CreateMap<Create, Astronaut>();
            }
        }

        public class Validator : AbstractValidator<Create>
        {
            public Validator()
            {
                RuleFor(a => a.Name).NotEmpty();
                RuleFor(a => a.Surname).NotEmpty();
                RuleFor(a => a.Birthdate)
                    .LessThanOrEqualTo(DateTime.Today)
                    .NotEmpty();
                RuleFor(a => a.Superpower).NotEmpty();
            }
        }
    }
}
