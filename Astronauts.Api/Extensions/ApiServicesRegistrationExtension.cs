﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Astronauts.Api.Versioning;

namespace Astronauts.Api.Extensions
{
    /// <summary>
    /// Registration of Api Services
    /// </summary>
    public static class ApiServicesRegistrationExtension
    {
        /// <summary>
        /// Collection of used services in the Api
        /// </summary>
        /// <param name="services">Collection of used services</param>
        /// <returns>Services that are used in the Api</returns>
        public static IServiceCollection AddApiServices(this IServiceCollection services)
        {
            var apiVersions = ApiVersionFinder.GetApiVersions();

            services.AddControllers()
                .AddDataAnnotationsLocalization()
                .AddApplicationPart(apiVersions)
                .AddControllersAsServices()
                .AddFluentValidation(configuration =>
                {
                    foreach (var item in apiVersions)
                    {
                        configuration.RegisterValidatorsFromAssemblyContaining(item);
                    }
                });
            services.AddApiVersioning(options => options.ReportApiVersions = true);
            services.AddConfiguredApiVersioningServices();
            services.AddConfiguredSwagger();
            services.AddAutoMapper(apiVersions);
            

            return services;
        }
    }
}