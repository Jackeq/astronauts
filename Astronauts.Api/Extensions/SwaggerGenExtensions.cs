using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Astronauts.Api.Extensions
{
    /// <summary>
    /// Setup swaggerGen
    /// JWT, Auth operations, Custom schema id
    /// </summary>
    public static class SwaggerGenExtensions
    {
        /// <summary>
        /// Integration of JWT configuration for Swagger UI.
        /// </summary>
        /// <param name="swaggerGenOptions"></param>
        /// <returns></returns>
        public static SwaggerGenOptions AddConfiguration(
            this SwaggerGenOptions swaggerGenOptions)
        {
            swaggerGenOptions.CustomSchemaIds(AstronautsSchemaIdSelector);
            
            return swaggerGenOptions;
        }

        /// <summary>
        /// Include Comment by class Type.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="types"></param>
        public static void IncludeXmlComments(this SwaggerGenOptions options, params Type[] types)
        {
            foreach (var type in types)
            {
                options.IncludeXmlComments(GetXmlCommentsFilePath(type));
            }
        }

        /// <summary>
        /// Get Assembly XmlComments file by Type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetXmlCommentsFilePath(Type type)
        {
            var basePath = PlatformServices.Default!.Application!.ApplicationBasePath!;
            var fileName = type.GetTypeInfo().Assembly.GetName().Name + ".xml";
            return Path.Combine(basePath, fileName);
        }

        private static string AstronautsSchemaIdSelector(Type modelType)
        {
            static string Normalize(Type type)
            {
                return $"{type.FullName!.Split("`").First().Split(".").Last()}".Replace("+", "For");
            }

            var type = Normalize(modelType);

            if (!modelType.IsGenericType)
            {
                return type;
            }

            foreach (var genericTypeArgument in modelType.GenericTypeArguments)
            {
                type += "Of" + Normalize(genericTypeArgument);
            }

            return type;
        }
    }
}