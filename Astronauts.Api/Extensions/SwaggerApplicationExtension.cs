using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace Astronauts.Api.Extensions
{
    /// <summary>
    /// Configure multiVersion swagger
    /// </summary>
    public static class SwaggerApplicationExtension
    {
        /// <summary>
        /// Configure swagger and swagger ui for each API version
        /// </summary>
        /// <param name="app"></param>
        /// <param name="versionDescriptionProvider"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseMultiVersionedSwagger(this IApplicationBuilder app,
            IApiVersionDescriptionProvider versionDescriptionProvider)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                // build a swagger endpoint for each discovered API version
                foreach (var description in versionDescriptionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName);
                }
            });
            return app;
        }
    }
}