using System;
using Microsoft.Extensions.DependencyInjection;

namespace Astronauts.Api.Extensions
{
    /// <summary>
    /// Register assemblies
    /// </summary>
    public static class MvcApplicationPartByTypeExtension
    {
        /// <summary>
        /// Add Application part for whole namespace by Type
        /// </summary>
        /// <param name="mvcBuilder"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public static IMvcBuilder AddApplicationPart(this IMvcBuilder mvcBuilder, params Type[] types)
        {
            foreach (var type in types)
            {
                if (type.Namespace != null)
                {
                    mvcBuilder.AddApplicationPart(type.Assembly);
                }
                else
                {
                    throw new DllNotFoundException("Type Namespace is empty.");
                }
            }

            return mvcBuilder;
        }
    }
}