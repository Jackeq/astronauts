using Astronauts.Api.Options;
using Astronauts.Api.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Astronauts.Api.Extensions
{
    /// <summary>
    /// Register and confifure swaggerGen
    /// </summary>
    public static class SwaggerRegistrationExtension
    {
        /// <summary>
        /// Register and configure swaggerGen
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddConfiguredSwagger(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(options =>
            {
                options.AddConfiguration();
                options.IncludeXmlComments(ApiVersionFinder.GetApiVersions());
            });
            return services;
        }
    }
}