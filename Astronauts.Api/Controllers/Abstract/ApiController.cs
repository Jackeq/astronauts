﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Astronauts.Api.Controllers.Abstract
{
    /// <summary>
    /// Basic api controller
    /// </summary>
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("api/[controller]")]
    public abstract class ApiController : ControllerBase
    {
        /// <summary>
        /// For mapping Entity to DTO
        /// </summary>
        protected IMapper Mapper { get; }

        /// <summary>
        /// Mapper everywhere!
        /// </summary>
        /// <param name="mapper"></param>
        protected ApiController(IMapper mapper)
        {
            Mapper = mapper;
        }
    } 
}