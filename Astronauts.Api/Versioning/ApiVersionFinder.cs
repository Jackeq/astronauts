using System;
using System.Linq;
using Astronauts.Api.Versioning;

namespace Astronauts.Api.Versioning
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApiVersionFinder
    {
        /// <summary>
        /// Lookup all Api version
        /// </summary>
        /// <returns></returns>
        public static Type[] GetApiVersions()
        {
            var type = typeof(IApiVersion);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p)).ToArray();
        }
    }
}