namespace Astronauts.Api.Versioning
{
    /// <summary>
    /// ApiVersion interface
    /// Assembly is registered to Api when contains ApiVersion implementation.
    /// </summary>
    public interface IApiVersion
    {
    }
}