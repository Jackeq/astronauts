﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Astronauts.Dal.Tests
{
    namespace Hotel.Dal.Tests.Api.V1.Helpers
    {
        public static class AstronautsContextTesting
        {
            private static SqliteConnection connection = null!;
            private static DbContextOptions<AstronautsContext> dbContextOptions = null!;
            private static readonly SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = ":memory:" };
			
            static AstronautsContextTesting()
            {
                Reconnect();
            }

            /// <summary>
            /// Reconnect database
            /// If you need recreate test data use.
            /// <see cref="RecreateDatabase"/>
            /// </summary>
            public static void Reconnect()
            {
                connection?.Close();
                connection = new SqliteConnection(connectionStringBuilder.ToString());
				
                connection.Open();

                dbContextOptions = new DbContextOptionsBuilder<AstronautsContext>().UseSqlite(connection)
                    .UseLazyLoadingProxies()
                    .Options;
            }
			
            /// <summary>
            /// Create close existing db connection, create new one and initialize test data.
            /// </summary>
            public static void RecreateDatabase()
            {
                Reconnect();
                using var context = CreateContext();
                context.Database.EnsureCreated();
            }

            /// <summary>
            /// Create new database context on existing connection.
            /// </summary>
            /// <returns></returns>
            public static AstronautsContext CreateContext()
            {
                return new AstronautsContext(dbContextOptions);
            }
        }
    }
}
