using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Astronauts.Runnable
{
    /// <summary>
    /// Application configuration
    /// </summary>
    public abstract class App
    {
        private static string Version { get; } = "v" + Assembly.GetEntryAssembly()?.GetName().Version;

        private const string AstronautsEnvironment = "Production";

        protected static string Environment = null!;

        protected readonly IConfiguration Configuration;

        protected readonly string[] Args;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <param name="environment">Should be compatible with <see cref="IWebHostEnvironment"/></param>
        public App(string[] args, string? environment)
        {
            Environment = environment ?? AstronautsEnvironment;
            Args = args;
            Configuration = CreateConfiguration(Environment);
        }

        /// <summary>
        /// Tries to run application
        /// </summary>
        /// <returns>number greater than 0 if error.</returns>
        public abstract Task<int> Run();

        /// <summary>
        /// Create configuration with default files.
        /// </summary>
        /// <returns>Loaded configuration</returns>
        public static IConfigurationRoot CreateConfiguration(string environment)
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddYamlFile("appsettings.yaml", false, true)
                .AddYamlFile($"appsettings.{Environment}.yaml", true, true)
                .AddYamlFile("appsettings.Local.yaml", true, true)
                .Build();
        }
    }
}